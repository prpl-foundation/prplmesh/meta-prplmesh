FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI = "git://gitlab.com/prpl-foundation/prplmesh/pwhm/plugins/pwhm.git;protocol=https;nobranch=1;"
SRC_URI_append += " file://pwhm.service \
                    file://wld_gen.sh"

SRCREV = "v5.47.0"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "pWHM"
LICENSE = "BSD-2-Clause-Patent.txt"
LIC_FILES_CHKSUM = "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "libwld"

DEPENDS = "libsahtrace libswla libswlc libamxo libamxm libnl"
RDEPENDS_${PN} = "libsahtrace libswla libswlc libamxo libamxm libnl amxrt libswla libswlc mod-netmodel mod-dmext"

CFLAGS += "-I${STAGING_DIR_TARGET}/usr/include/libnl3"

do_install_append() {
    install -d ${D}${sysconfdir}/init.d/
    install -m 0755 ${WORKDIR}/wld_gen.sh ${D}${sysconfdir}/init.d/wld_gen
    install -D -p -m0644 ${WORKDIR}/pwhm.service ${D}${systemd_unitdir}/system/pwhm.service
}

FILES_SOLIBSDEV = ""

FILES_${PN}-dev += "${includedir}/wld/*.h"
FILES_${PN} += "${libdir}/${COMPONENT}${SOLIBSDEV}"
FILES_${PN} += "${libdir}/amx/wld/*.sh"
FILES_${PN} += "${libdir}/debuginfo/*.sh"
FILES_${PN} += "${libdir}/amx/wld/wld${SOLIBSDEV}"
FILES_${PN} += "${libdir}/amx/wld/wld${SOLIBS}"
FILES_${PN} += "${libdir}/${COMPONENT}${SOLIBS}"
FILES_${PN} += "${systemd_unitdir}/system/pwhm.service"

INSANE_SKIP:${PN} += "dev-so"

EXTRA_OEMAKE += "STAGINGDIR=${STAGING_DIR_TARGET}"