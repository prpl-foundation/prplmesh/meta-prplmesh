# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: 2020 the prplMesh contributors (see AUTHORS.md)
# This code is subject to the terms of the BSD+Patent license.
# See LICENSE file for more details.
#
# Variables specific for Vanilla image on Puma7 board
# Notes about "Vanilla" image:
# - no RDKB
# - Yocto Poky based
# - no Gateway, board works in bridge mode
# - Packet Processor is managed by ARM
# 
# By default prplmesh is disabled on "Vanilla" images
# See this meta layer README how enable it
#
TARGET_PLATFORM_cougarmountain ?= "linux"
BWL_TYPE_cougarmountain ?= "bwl-dwpal"

BEEROCKS_BRIDGE_IFACE_cougarmountain ?= "brwlan0"
BEEROCKS_BH_WIRE_IFACE_cougarmountain ?= "eth0"
