# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: 2020 the prplMesh contributors (see AUTHORS.md)
# This code is subject to the terms of the BSD+Patent license.
# See LICENSE file for more details.

# This file has been made as .inc instead of .bb to allow main prplmesh
# recipe influence on it's via variables 

inherit systemd

SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE_${PN} = "prplmesh.service"

SRC_URI += "file://prplmesh.service"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"

SYSTEMD_UNIT_DEPS ??= "hostapd.service ${@bb.utils.contains('PACKAGECONFIG', 'nbapi', 'ubusd.service', '', d)}"

do_install_append () {
    install -d ${D}${systemd_unitdir}/system/
    install -m 0644 ${WORKDIR}/prplmesh.service ${D}${systemd_unitdir}/system

    sed -i -e 's,@@INSTALL_PREFIX,${INSTALL_PREFIX},g' \
           -e 's,@@SYSTEMD_UNIT_DEPS,${SYSTEMD_UNIT_DEPS},g' \
           ${D}${systemd_unitdir}/system/prplmesh.service
}

FILES_${PN} += "${systemd_unitdir}/system/prplmesh.service"
