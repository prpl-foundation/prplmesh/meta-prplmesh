# SPDX-License-Identifier: BSD-2-Clause-Patent
#
# SPDX-FileCopyrightText: 2020 the prplMesh contributors (see AUTHORS.md)
#
# This code is subject to the terms of the BSD+Patent license.
# See LICENSE file for more details.
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

inherit ${@d.getVar('DISTRO', True) == 'rdk' and 'systemd' or 'base'}

SRC_URI_append_rdk += " file://ubusd.service"

SYSTEMD_SERVICE_${PN}_rdk = "ubusd.service"

do_install_append_rdk() {
    # Install systemd unit files
    install -d ${D}${systemd_unitdir}/system
    install -m 0644 ${WORKDIR}/ubusd.service ${D}${systemd_unitdir}/system
        sed -i -e 's,@SBINDIR@,${sbindir},g' \
            ${D}${systemd_unitdir}/system/ubusd.service
}

FILES_${PN}d += "${sbindir} ${base_sbindir}"
FILES_${PN}_remove += "/usr/sbin/* /sbin/*"
